//
//  RNTest.m
//  RNTest
//
//  Created by Ari Goldberg on 2/4/16.
//  Copyright © 2016 Ari Goldberg. All rights reserved.
//

#import "RNTest.h"

@implementation RNTest

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(echo:(NSString *)msg) {
    NSLog(msg);
}

@end
