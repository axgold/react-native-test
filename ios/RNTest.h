//
//  RNTest.h
//  RNTest
//
//  Created by Ari Goldberg on 2/4/16.
//  Copyright © 2016 Ari Goldberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PushKit/PushKit.h>

#import "RCTBridgeModule.h"

@interface RNTest : NSObject <RCTBridgeModule>

@end
