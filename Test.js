'use strict';

var RNTest = require('react-native').NativeModules.RNTest;

class Test {
    static echo(msg) {
        RNTest.echo(msg);   
    }
}

module.exports = Test;